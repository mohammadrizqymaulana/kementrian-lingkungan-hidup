package abhimantra.kementrianlingkunganhidup.JSON;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import abhimantra.kementrianlingkunganhidup.Model.ModelActivity;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Rizqy on 8/24/2016.
 */
public interface JSONActivity {
     String BASE_URL = "http://sikoproject.org/KLHK/";

    @FormUrlEncoded
    @POST("insert.php")
    Call<ModelActivity> submit(@Field("kd_lokasi") String username,
                               @Field("nama_pengirim") String pengirim,
                               @Field("tgl") String survey,
                               @Field("tm_air") String tinggi,
                               @Field("kelembaban") String kelembaban,
                               @Field("curah_hujan") String curah);



    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(new OkHttpClient.Builder().readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build())
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
            .build();
}



package abhimantra.kementrianlingkunganhidup;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;

import abhimantra.kementrianlingkunganhidup.JSON.JSONActivity;
import abhimantra.kementrianlingkunganhidup.Model.ModelActivity;
import abhimantra.kementrianlingkunganhidup.Utility.DataSingleton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity implements  View.OnClickListener {
    ProgressDialog dialog = null;
    @BindView(R.id.lokasi)
    EditText lokasi;
    @BindView(R.id.pengirim)
    EditText pengirim;
    @BindView(R.id.tanggal)
    EditText tanggal;
    @BindView(R.id.air)
    EditText air;
    @BindView(R.id.kelembaban)
    EditText kelembaban;
    @BindView(R.id.hujan)
    EditText hujan;
    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;
    private int mYear, mMonth, mDay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        final Calendar c = Calendar.getInstance();
        selectedYear = c.get(Calendar.YEAR);
        selectedMonth = c.get(Calendar.MONTH);
        selectedDate = c.get(Calendar.DAY_OF_MONTH);
        DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                selectedDate);
        tanggal.setText(DataSingleton.getInstance().getFormattedDate());
        tanggal.setOnClickListener(this);
    }

    @OnClick(R.id.konfirm)
    public void Konfirm(){

    }

    @OnClick(R.id.sms)
    public void KirimSMS(){

    }

    @OnClick(R.id.data)
    public void KirimData(){
        submit();
    }

    private void submit() {
        dialog = ProgressDialog.show(MainActivity.this, "",
                "Data Sedang Dikirim...", true);
        JSONActivity in = JSONActivity.retrofit.create(JSONActivity.class);
        in.submit(lokasi.getText().toString(), pengirim.getText().toString(), tanggal.getText().toString(),
                air.getText().toString(), kelembaban.getText().toString(), hujan.getText().toString())
                .enqueue(new Callback<ModelActivity>() {
                    @Override
                    public void onResponse(Call<ModelActivity> call, Response<ModelActivity> response) {
                        dialog.dismiss();

                        ModelActivity user = response.body();

                        if (user.getSuccess()) {

                            Toast.makeText(MainActivity.this, "Data Berhasil Dikirim", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelActivity> call, Throwable t) {
                        dialog.dismiss();

                        Toast.makeText(MainActivity.this, "Data Gagal Dikirim", Toast.LENGTH_SHORT).show();



                    }
                });


    }

    @Override
    public void onClick(View v) {
        if (v == tanggal) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            tanggal.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
    }
}

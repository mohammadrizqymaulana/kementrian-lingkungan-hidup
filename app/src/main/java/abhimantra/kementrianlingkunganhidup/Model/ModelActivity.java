package abhimantra.kementrianlingkunganhidup.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizqy on 8/24/2016.
 */
public class ModelActivity {
    @SerializedName("kd_lokasi")
    private String kode_lokasi;
    @SerializedName("nama_pengirim")
    private String nama_pengirim;
    @SerializedName("tgl")
    private String tgl_survey;
    @SerializedName("tm_air")
    private String tinggi_MAT;
    @SerializedName("kelembaban")
    private String kelembaban;
    @SerializedName("curah_hujan")
    private String curah_hujan;

    @SerializedName("success")
    private boolean success;
    @SerializedName("result")
    private String result;



    public String getKode_lokasi() {
        return kode_lokasi;
    }

    public void setKode_lokasi(String kode_lokasi) {
        this.kode_lokasi = kode_lokasi;
    }

    public String getNama_pengirim() {
        return nama_pengirim;
    }

    public void setNama_pengirim(String nama_pengirim) {
        this.nama_pengirim = nama_pengirim;
    }

    public String getTgl_survey() {
        return tgl_survey;
    }

    public void setTgl_survey(String tgl_survey) {
        this.tgl_survey = tgl_survey;
    }

    public String getTinggi_MAT() {
        return tinggi_MAT;
    }

    public void setTinggi_MAT(String tinggi_MAT) {
        this.tinggi_MAT = tinggi_MAT;
    }

    public String getKelembaban() {
        return kelembaban;
    }

    public void setKelembaban(String kelembaban) {
        this.kelembaban = kelembaban;
    }

    public String getCurah_hujan() {
        return curah_hujan;
    }

    public void setCurah_hujan(String curah_hujan) {
        this.curah_hujan = curah_hujan;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public String getResult() {
        return result;
    }
    public void setResult(String result) {

        this.result = result;
    }
}

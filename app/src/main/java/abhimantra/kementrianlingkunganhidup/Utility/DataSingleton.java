package abhimantra.kementrianlingkunganhidup.Utility;

import android.os.Handler;

/**
 * Created by Rizqy on 8/12/2016.
 */
public class DataSingleton {

    private String inspectorName;
    private String[] otherInspectors;
    private static DataSingleton instance;

    static private int selectedYear;
    static private int selectedMonth;
    static private int selectedDay;
    static private int selectedHour;
    static private int selectedMinute;
    private boolean logTime = false;
    private boolean isAgreeSOP = false;

    public static String pathImageFieldInspection;
    public static int totalRowFieldInspection = 0;

    int t = 1;
    long starttime = 0L;
    Handler handler = new Handler();
    long timeSwapBuff = 0L;
    long timeInMilliseconds = 0L;
    private static final String LOGT_TIME= "log_time";

    protected DataSingleton() {

    }

    public static DataSingleton getInstance() {
        if (instance == null) {
            instance = new DataSingleton();
        }

        return instance;
    }

    public static String getFormattedDateTime() {
        return Helper.formatNumber(selectedDay) + " "
                + Helper.formatNumber(selectedMonth + 1) + " "
                + Helper.formatNumber(selectedYear) + " "
                + Helper.formatNumber(selectedHour) + ":"
                + Helper.formatNumber(selectedMinute);
    }

    public String getSHEFormattedDateTime() {
        return Helper.formatNumber(selectedYear)
                + Helper.formatNumber(selectedMonth)
                + Helper.formatNumber(selectedDay)
                + Helper.formatNumber(selectedHour)
                + Helper.formatNumber(selectedMinute);
    }

    public String getFormattedDate() {
        return Helper.formatNumber(selectedYear) + "-"
                + Helper.formatNumber(selectedMonth + 1) + "-"
                + Helper.formatNumber(selectedDay);
    }

    public static String getFormattedTime() {
        return Helper.formatNumber(selectedHour) + ":"
                + Helper.formatNumber(selectedMinute);
    }

    public String[] getSplittedFormattedDate(String formattedDate){
        return formattedDate.split(" ");
    }

    public void setDateTime(int year, int month, int day, int hour, int minutes) {
        selectedDay = day;
        selectedHour = hour;
        selectedYear = year;
        selectedMonth = month;
        selectedMinute = minutes;
    }

    public void setTime(int hour, int minutes) {
        selectedHour = hour;
        selectedMinute = minutes;
    }

    public void setDate(int year, int month, int day) {
        selectedDay = day;
        selectedYear = year;
        selectedMonth = month;
    }

    public String getInspectorName() {
        return inspectorName;
    }

    public void setInspectorName(String inspectorNameP) {
        inspectorName = inspectorNameP;
    }

    public String[] getOtherInspectors() {
        return otherInspectors;
    }

    public void setOtherInspectors(String[] otherInspectorsP) {
        otherInspectors = otherInspectorsP;
    }

    public boolean logTime(){
        return logTime;
    }
    public void setlogTime(boolean logTime){
        this.logTime = logTime;
    }

    public boolean isAgreeSOP() {
        return isAgreeSOP;
    }



    public void setAgreeSOP(boolean isAgreeSOP) {
        this.isAgreeSOP = isAgreeSOP;
    }

    public String getImagePath() {
        return pathImageFieldInspection;
    }

    public void setImagePath(String imagePath) {
        pathImageFieldInspection = imagePath;
    }

    public int getTotalRowFieldInspection() {
        return totalRowFieldInspection;
    }

    public void resetTotalRowFieldInspection() {
        totalRowFieldInspection = 0;
    }

    public void addTotalRowFieldInspection() {
        totalRowFieldInspection++;
    }

    public void removeTotalRowFieldInspection() {
        if (totalRowFieldInspection > 0)
            totalRowFieldInspection--;
    }

    public String getDateYYYYMMDD() {
        return Helper.formatNumber(selectedYear)
                + Helper.formatNumber(selectedMonth + 1)
                + Helper.formatNumber(selectedDay);
    }
}

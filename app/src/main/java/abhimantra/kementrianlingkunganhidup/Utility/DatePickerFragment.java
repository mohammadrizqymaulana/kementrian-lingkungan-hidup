package abhimantra.kementrianlingkunganhidup.Utility;//package abhimantra.kementrianlingkunganhidup.Utility;
//
//import android.app.DatePickerDialog;
//import android.app.Dialog;
//import android.os.Bundle;
//import android.support.v4.app.DialogFragment;
//import android.support.v4.app.FragmentActivity;
//
//import java.util.Calendar;
//
///**
// * Created by Rizqy on 8/12/2016.
// */
//public class DatePickerFragment extends DialogFragment {
//    private DatePickerDialog.OnDateSetListener onDateSetListener;
//    private FragmentActivity fragmentActivity;
//    private int year;
//    private int month;
//    private int day;
//
//    public DatePickerFragment(FragmentActivity fragmentActivity, DatePickerDialog.OnDateSetListener onDateSetListener){
//        this.onDateSetListener = onDateSetListener;
//        this.fragmentActivity = fragmentActivity;
//
//        // Use the current date as the default date in the picker
//        final Calendar c = Calendar.getInstance();
//
//
//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH);
//        day = c.get(Calendar.DAY_OF_MONTH);
//    }
//
//    public DatePickerFragment(FragmentActivity fragmentActivity, DatePickerDialog.OnDateSetListener callback, int year, int month, int day){
//        this.onDateSetListener = callback;
//        this.fragmentActivity = fragmentActivity;
//        this.year = year;
//        this.month = month;
//        this.day = day;
//    }
//
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        // Create a new instance of DatePickerDialog and return it
//        return new DatePickerDialog(fragmentActivity, onDateSetListener, year, month, day);
//    }
//}
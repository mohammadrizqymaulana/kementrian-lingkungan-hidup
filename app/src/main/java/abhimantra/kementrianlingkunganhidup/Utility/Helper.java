package abhimantra.kementrianlingkunganhidup.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by Rizqy on 8/12/2016.
 */
public class Helper {





    public static ProgressDialog progressDialog;
    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;
    static String extStorageDirectory = Environment
            .getExternalStorageDirectory().getAbsolutePath();

    public static void checkAndCreateDirectory(String pathToDirectory) {
        File directory = new File(pathToDirectory);
        if (!(directory.exists() && directory.isDirectory())) {
            directory.mkdirs();
        }
    }
    public enum LookupType {
        //			Fresh, Department, Section, BussinessUnit, Company,
        Work_Order, Inspector
    }
    public static boolean isNotNull(String txt) {
        return txt != null && txt.trim().length() > 0 ? true : false;
    }

    public static String formatNumber(int number) {
        if (number < 10) {
            return "0" + String.valueOf(number);
        } else {
            return String.valueOf(number);
        }
    }

    public static void showProgressDialog(Context context) {
        progressDialog = ProgressDialog.show(context, "Loading",
                "Please Wait..", true, false);
    }

    public static void showProgressDialog(Context context, String title,
                                          String message) {
        progressDialog = ProgressDialog.show(context, title, message, true,
                false);
    }

    public static String numToMonthName(int num) {
        switch (num) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "January";
        }
    }


    public static String appBaseDirectory = Environment
            .getExternalStorageDirectory().getAbsolutePath()
            + "/Sucofindo/DraughtSurvey";


    public static void deleteAllFilesOnDirectory(String directoryPath) {
        if (directoryPath != null) {
            if (directoryPath.length() > 0) {
                File dir = new File(directoryPath);
                if (dir.exists() && dir.isDirectory()) {
                    String[] files = dir.list();
                    if (files.length > 0) {
                        for (int i = 0; i < files.length; i++) {
                            new File(directoryPath, files[i]).delete();
                        }
                    }
                }
            }
        }
    }
    public static void copyFile(InputStream in, OutputStream out)
            throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
    public static String bitmapToString(Bitmap bitmap){
        String encodedImage = null;
        if(bitmap != null){
            try{
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();
                encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return encodedImage;
    }

    public static Bitmap stringToBitmap(String imageString){
        Bitmap bitmap = null;
        if(imageString != null){
            byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        }

        return bitmap;
    }


    public static boolean isAvailableIntent(Context ctx, Intent intent) {
        final PackageManager mgr = ctx.getPackageManager();
        List<ResolveInfo> list = mgr.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }
    public static void copyDirectory(File sourceLocation , File targetLocation) {
        try{
            if (sourceLocation.isDirectory()) {
                if (!targetLocation.exists() && !targetLocation.mkdirs()) {
//			            throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
                } else{
                    String[] children = sourceLocation.list();
                    for (int i=0; i<children.length; i++) {
                        copyDirectory(new File(sourceLocation, children[i]),
                                new File(targetLocation, children[i]));
                    }
                }
            } else {
                // make sure the directory we plan to store the recording in exists
                File directory = targetLocation.getParentFile();
                if (directory != null && !directory.exists() && !directory.mkdirs()) {
//			            throw new IOException("Cannot create dir " + directory.getAbsolutePath());
                } else{
                    if(sourceLocation.getPath().equalsIgnoreCase(targetLocation.getPath())){

                    } else{
                        InputStream in = new FileInputStream(sourceLocation);
                        OutputStream out = new FileOutputStream(targetLocation);

                        // Copy the bits from instream to outstream
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = in.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }
                        in.close();
                        out.close();
                    }
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public static void setVersionName(Activity activity, TextView tvVersionName) {
        String versionName;
        try {
            versionName = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = "version name cannot be detected";
        }
        tvVersionName.setText("Version : " + versionName);
    }

    public static void showPositiveNegativeDialog(Activity activity,
                                                  String title, String message, String positiveText,
                                                  String negativeText,
                                                  DialogInterface.OnClickListener positiveOnClickLister,
                                                  DialogInterface.OnClickListener negativeOnClickLister) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) {
            builder.setTitle(title);
        }

        if (message != null) {
            builder.setMessage(message);
        }
        builder.setCancelable(false);
        builder.setPositiveButton(positiveText != null ? positiveText : "OK",
                positiveOnClickLister);
        builder.setNegativeButton(negativeText != null ? negativeText
                : "Cancel", negativeOnClickLister);
        builder.show();
    }

    public static Bitmap getBitmapFromFile(String imagePath) {
        Bitmap bitmap = null;

        if (imagePath != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inSampleSize = 8;
            bitmap = BitmapFactory.decodeFile(imagePath, options);
        }

        return bitmap;
    }

    public static Bitmap rotateImage(Bitmap img, float degree) {
        Matrix matrix = new Matrix();
        matrix.setRotate(degree, img.getWidth() / 2, img.getHeight() / 2);

        Bitmap rotatedBMP = Bitmap.createBitmap(img, 0, 0, img.getWidth(),
                img.getHeight(), matrix, true);

        return rotatedBMP;
    }

    @SuppressWarnings("deprecation")
    public static Bitmap rotateImage(String filePath, float degree, int reqWidth) {
        Bitmap rotatedBMP = null;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        bmOptions.inSampleSize = 5;
        bmOptions.inPurgeable = true;
        Bitmap img;
        BitmapFactory.decodeFile(filePath, bmOptions);

        // Calculate inSampleSize
        bmOptions.inSampleSize = 8;// calculateInSampleSize(bmOptions,
        // reqWidth);

        // Decode bitmap with inSampleSize set
        bmOptions.inJustDecodeBounds = false;
        img = BitmapFactory.decodeFile(filePath, bmOptions);

        Matrix matrix = new Matrix();
        matrix.setRotate(degree, img.getWidth() / 2, img.getHeight() / 2);

        rotatedBMP = Bitmap.createBitmap(img, 0, 0, img.getWidth(),
                img.getHeight(), matrix, true);

        return rotatedBMP;
    }

    public static Bitmap rotateImageAccordingToImageOrientation(
            String imagePath, int reqWidth) {
        Bitmap bitmap = null;
        if (imagePath != null) {
            try {
                ExifInterface ei;
                ei = new ExifInterface(imagePath);
                int orientation = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = Helper.rotateImage(imagePath, 90, (int) reqWidth);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = Helper.rotateImage(imagePath, 180, (int) reqWidth);
                        break;
                    default:
                        bitmap = Helper.rotateImage(imagePath, 0, (int) reqWidth);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }
    public static void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles())
                delete(c);
        }
        if (!f.delete())
            throw new FileNotFoundException("Failed to delete file: " + f);
    }
//    public static void showDatePicker(View v, FragmentActivity context, DatePickerDialog.OnDateSetListener callBack) {
//        DialogFragment newFragment = new DatePickerFragment(context, callBack);
//        newFragment.show(context.getSupportFragmentManager(), "datePicker");
//    }
//
//    public static void showDatePicker(View v, FragmentActivity context, DatePickerDialog.OnDateSetListener callBack, int year, int month, int day) {
//        DialogFragment newFragment = new DatePickerFragment(context, callBack, year, month, day);
//        newFragment.show(context.getSupportFragmentManager(), "datePicker");
//    }


}